from django.shortcuts import render, HttpResponse, redirect
from django.contrib import messages
from .models import *

def index(request):
    # return HttpResponse("you did")
    return render(request, "log.html")

def success(request):
    if 'name' not in request.session:
        return redirect('/')
    context = {
        'all_messages': Wall_Message.objects.all(),
        'wishes': Wish.objects.all()
    }
    return render(request, "success.html", context)

def register(request):
    if request.method == 'POST':
        errors = User.objects.validator(request.POST)
        print(errors)
        if len(errors) > 0:
            for key, values in errors.items():
                messages.error(request, values)
            return redirect('/')
        new_user = User.objects.create(first_name=request.POST['first_name'], last_name=request.POST['last_name'], email=request.POST['email'], password=request.POST['password'])
        request.session['name'] = new_user.first_name
        request.session['user_id'] = new_user.id
        return redirect('/success')
    return redirect('/')

def login(request):
    if request.method == 'POST':
        logged_user = User.objects.filter(email=request.POST['email'])
        if len(logged_user) > 0:
            logged_user = logged_user[0]
            if logged_user.password == request.POST['password']:
                request.session['name'] = logged_user.first_name
                request.session['user_id'] = logged_user.id
                return redirect('/success')
    return redirect('/')

def logout(request):
    request.session.flush()
    return redirect('/')

## WALL FUNCTIONALITY ##

# add a message
def add_message(request):
    # make sure this is a post request
    if request.method == 'POST':
        # validate our message!
        errors = Wall_Message.objects.validator(request.POST)
        if errors:
            # throw them into messages
            for key, values in errors.items():
                messages.error(request, values) 
            return redirect('/success')
        # create the message
        new_mess = Wall_Message.objects.create(content=request.POST['content'], poster=User.objects.get(id=request.session['user_id']))
        return redirect('/success')
    return redirect('/')

def profile(request, id):
    context = {
        'one_user': User.objects.get(id=id)
    }
    return render(request, 'profile.html', context)

def new(request):
    context = {
        'user': User.objects.all()
    }
    return render(request, 'new.html', context)

def create_wish(request):
    new_wish = Wish.objects.create(item=request.POST['item'], description=request.POST['description'], user=User.objects.get(id=request.session['user_id']))
    return redirect('/success')

def delete(request, wish_id):
    to_delete = Wish.objects.get(id=wish_id)
    to_delete.delete()
    return redirect('/success')

def edit_wish(request, id):
    context = {
    'user': User.objects.get(id=id),
    'wish': Wish.objects.get(id=id)
    }
    return render(request, 'edit.html', context)

def update(request, id):
    if request.method == 'POST':
        wish = Wish.objects.get(id=id)
        wish.item = request.POST['item']
        wish.description = request.POST['description']
        wish.save()
        return redirect('/success')
    return redirect('/success')

