from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.index), # landing page
    path('register', views.register), # form-action, creating a user
    path('login', views.login), # form-action, verifying a current user and sending them to login-success page
    path('success', views.success), # login-success page
    path('logout', views.logout), # hyperlink-action, logging a user out of session
    ## WALL PATHS
    path('create_message', views.add_message),
    path('profile/<int:id>', views.profile),
    ## WISH PATHS
    path('new', views.new),
    path('create_wish', views.create_wish),
    path('<int:wish_id>/delete', views.delete),
    path('<int:id>/edit/', views.edit_wish),
    path('update/<int:id>', views.update)
]
