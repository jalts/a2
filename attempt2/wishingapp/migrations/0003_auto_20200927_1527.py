# Generated by Django 3.1.1 on 2020-09-27 22:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('wishingapp', '0002_granted_wish_wish'),
    ]

    operations = [
        migrations.AlterField(
            model_name='wish',
            name='description',
            field=models.CharField(max_length=255),
        ),
    ]
