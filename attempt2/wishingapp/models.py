from django.db import models
import re

class UserManager(models.Manager):
    def validator(self, postData):
        errors = {}
        EMAIL_REGEX = re.compile(r'^[a-zA-Z0-9.+_-]+@[a-zA-Z0-9._-]+\.[a-zA-Z]+$')
        if len(postData['first_name']) < 2:
            errors['first_name'] = "your first name must be more than two chars"
        if len(postData['last_name']) < 2:
            errors['last_name'] = "your first name must be more than two chars"
        if not EMAIL_REGEX.match(postData['email']):
            errors['email'] = "email must be valid format"
        if len(postData['password']) < 8:
            errors['password'] = 'password must be at least 8 chars'
        if postData['password'] != postData['confirm_password']:
            errors['confirm_password'] = "passwords do not match"
        return errors
        
class User(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    password = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = UserManager()

class MessageManager(models.Manager):
    def validator(self, form):
        errors = {}
        if len(form['content']) < 5:
            errors['length'] = "message must be ast least 5 chars!"
        return errors

class Wall_Message(models.Model):
    content = models.CharField(max_length=255)
    poster = models.ForeignKey(User, related_name="messages", on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = MessageManager()

class Wish(models.Model):
    item = models.CharField(max_length=255)
    description = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User, related_name="wishes", on_delete=models.CASCADE)

class Granted_wish(models.Model):
    item = models.CharField(max_length=255)
    date_added = models.DateTimeField(auto_now=True)
    granted_at = models.DateTimeField(auto_now_add=True)
    likes = models.ManyToManyField(User, related_name='likes')
    user = models.ForeignKey(User, related_name="granted_wishes", on_delete=models.CASCADE)